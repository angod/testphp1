<?php
  namespace App\Controller;

  use Symfony\Component\HttpFoundation\Response;
  use Symfony\Component\Routing\Annotation\Route;
  use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

  class MainpageController extends AbstractController
  {
    /**
    * @Route("/")
    */
    public function show()
    {
      return $this->render("mainpage.html.twig");
    }
  }
?>