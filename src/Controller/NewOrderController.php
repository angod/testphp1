<?php
  namespace App\Controller;

  use App\Entity\User;
  use App\Entity\AccountType;

  use Symfony\Component\HttpFoundation\Request;
  use Doctrine\ORM\EntityManagerInterface;
  use Symfony\Component\HttpFoundation\Response;
  use Symfony\Component\Routing\Annotation\Route;
  use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

  class NewOrderController extends AbstractController
  {
    /**
    * @Route("/orders/new")
    */
    public function addOrder()
    {
      return $this->render("neworder.html.twig",
        [
        "user_id" => $_GET["user_id"]
        ]);
    }
  }
?>