<?php
  namespace App\Controller;

  use App\Entity\User;
  use App\Entity\AccountType;
  use App\Entity\Ord;

  use Symfony\Component\HttpFoundation\Request;
  use Doctrine\ORM\EntityManagerInterface;
  use Symfony\Component\HttpFoundation\Response;
  use Symfony\Component\Routing\Annotation\Route;
  use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
  use Symfony\Component\HttpFoundation\JsonResponse;

  class OrdersController extends AbstractController
  {
    /**
    * @Route("/orders/take")
    */
    public function isTaken()
    {
      $entityManager = $this->getDoctrine()->getManager();

      $contractor = new User();
      $ord = new Ord();

      if (isset($_POST["action"]))
      {
        $action = $_POST["action"];
        $ordId = $_POST["ordid"];
        $contractorId = $_POST["userid"];

        $contractor = $this->getDoctrine()
          ->getRepository(User::class)
          ->findOneBy(["id" => $contractorId]);

        if ($action === "take")
        {
          $ord = $this->getDoctrine()
            ->getRepository(Ord::class)
            ->findOneBy(["contractor" => $contractor]);

          if ($ord)
          {
            return new JsonResponse([
              "status" => "FAILURE",
              "message" => "Only one active order available"
            ]);
          }
        }

        $ord = $this->getDoctrine()
          ->getRepository(Ord::class)
          ->findOneBy(["id" => $ordId]);

        if (!$ord->getIsActive())
        {
          $ord->setContractor($contractor);
          $ord->setIsActive(true);

          $entityManager->persist($ord);
          $entityManager->flush();
        }
        else
        {
          $ord->setContractor(null);
          $ord->setIsActive(false);

          $entityManager->persist($ord);
          $entityManager->flush();
        }

        return new JsonResponse([
          "status" => "SUCCESS",
          "message_1" => "ordid: {$ordId}",
          "message_2" => "contractorid: {$contractorId}",
          "message_3" => "ord: {$ord->getOrdname()}",
          "message_4" => "contractor: {$contractor->getUsername()}",
          "message_5" => "order status: {$ord->getIsActive()}"
          ]);
      }
      else
      {
        return new JsonResponse([
          "status" => "FAILURE",
          "message" => "something went wrong"]);
      }
    }

    /**
    * @Route("/orders")
    */
    public function showOrders()
    // public function orders(Request $request)
    {
      $user = new User();
      $accountType = new AccountType();

      $entityManager = $this->getDoctrine()->getManager();

      if (isset($_POST["ordid"]))
      {
        return new JsonResponse(["message" => "AJAX"]);
      }
      // else
      // {
      //   return new JsonResponse(["message" => "ajax"]);
      // }

      if ($_POST["form_id"] === "sup" || $_POST["form_id"] === "sin")
      {
        $uname = $_POST["username"];
        $passwd = $_POST["password"];
      }

      // data from signup page
      if ($_POST["form_id"] === "sup")
      {

        $isExist = $this->getDoctrine()
          ->getRepository(User::class)
          ->findBy(["username" => $uname]);

        if ($isExist)
        {
          return new Response("This username not available");
        }


        $user->setUsername($uname);
        $user->setPassword($passwd);

        $accountType->setUsername($user);
        if ($_POST["account_type"] === "customer")
          $accountType->setIsCustomer(true);
        else
          $accountType->setIsContractor(true);

        // insert into DB
        $entityManager->persist($user);
        $entityManager->persist($accountType);

        $entityManager->flush();
      }

      // data from singin page
      if ($_POST["form_id"] === "sin")
      {
        $isLogin = $this->getDoctrine()
          ->getRepository(User::class)
          ->findOneBy(["username" => $uname, "password" => $passwd]);

        if (!$isLogin)
        {
          return new Response("Authorization is failed");
        }

        $user = $this->getDoctrine()
          ->getRepository(User::class)
          ->findOneBy(["username" => $uname]);

        $accountType = $this->getDoctrine()
          ->getRepository(AccountType::class)
          ->findOneBy(["username" => $user->getId()]);
      }

      // data from new_order page
      if ($_POST["form_id"] === "new_order")
      {
        $user = $this->getDoctrine()
          ->getRepository(User::class)
          ->findOneBy(["id" => $_POST["user_id"]]);

        $accountType = $this->getDoctrine()
          ->getRepository(AccountType::class)
          ->findOneBy(["username" => $user->getId()]);

        $order = new Ord();
        $order->setCustomer($user);
        $order->setOrdname($_POST["ordname"]);
        $order->setOrddesc($_POST["orddesc"]);
        $order->setOrdprice($_POST["ordprice"]);

        $entityManager->persist($order);
        $entityManager->flush();
      }

      if ($accountType->getIsCustomer())
      {
        $orders = $this->getDoctrine()
          ->getRepository(Ord::class)
          ->findBy(["customer" => $user->getId()]);
      }

      if ($accountType->getIsContractor())
      {
        $orders = $this->getDoctrine()
          ->getRepository(Ord::class)
          ->findAll();
      }

      return $this->render("orders.html.twig",
        [
        "account_type" => $accountType,
        "user_id" => $user->getId(),
        "orders" => $orders,
        ]);
    }
  }
?>