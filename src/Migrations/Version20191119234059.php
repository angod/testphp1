<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191119234059 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ord (id INT AUTO_INCREMENT NOT NULL, customer_id INT NOT NULL, contractor_id INT DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, ordname VARCHAR(255) NOT NULL, orddesc VARCHAR(255) NOT NULL, ordprice VARCHAR(255) NOT NULL, INDEX IDX_EB1CEB3A9395C3F3 (customer_id), INDEX IDX_EB1CEB3AB0265DC7 (contractor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ord ADD CONSTRAINT FK_EB1CEB3A9395C3F3 FOREIGN KEY (customer_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE ord ADD CONSTRAINT FK_EB1CEB3AB0265DC7 FOREIGN KEY (contractor_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE ord');
    }
}
