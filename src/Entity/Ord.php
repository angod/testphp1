<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrdRepository")
 */
class Ord
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", fetch="EAGER")
     */
    private $contractor;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_active;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ordname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $orddesc;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ordprice;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomer(): ?User
    {
        return $this->customer;
    }

    public function setCustomer(?User $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getContractor(): ?User
    {
        return $this->contractor;
    }

    public function setContractor(?User $contractor): self
    {
        $this->contractor = $contractor;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getOrdname(): ?string
    {
        return $this->ordname;
    }

    public function setOrdname(string $ordname): self
    {
        $this->ordname = $ordname;

        return $this;
    }

    public function getOrddesc(): ?string
    {
        return $this->orddesc;
    }

    public function setOrddesc(string $orddesc): self
    {
        $this->orddesc = $orddesc;

        return $this;
    }

    public function getOrdprice(): ?string
    {
        return $this->ordprice;
    }

    public function setOrdprice(string $ordprice): self
    {
        $this->ordprice = $ordprice;

        return $this;
    }
}
