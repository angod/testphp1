<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountTypeRepository")
 */
class AccountType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\user", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $username;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $IsCustomer;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $IsContractor;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?user
    {
        return $this->username;
    }

    public function setUsername(user $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getIsCustomer(): ?bool
    {
        return $this->IsCustomer;
    }

    public function setIsCustomer(bool $IsCustomer): self
    {
        $this->IsCustomer = $IsCustomer;

        return $this;
    }

    public function getIsContractor(): ?bool
    {
        return $this->IsContractor;
    }

    public function setIsContractor(?bool $IsContractor): self
    {
        $this->IsContractor = $IsContractor;

        return $this;
    }
}
